FROM python:3.8-slim
LABEL maintainer="Gert Van Gool <gert@phyx.be>"

RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y \
        make\
        wget \
        texlive \
        texlive-latex-extra \
        latexmk \
        dvipng \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN pip install sphinx \
    sphinx-autobuild \
    sphinx_bootstrap_theme \
    sphinx_rtd_theme \
    lxml
