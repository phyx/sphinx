GitLab CI Sphinx Builder
========================
This is a simple container that will allow you to generate your Sphinx
documentation from GitLab CI.

Basic usage::

  image: registry.gitlab.com/phyx/sphinx:master

  build:
    artifacts:
      paths:
        - _build/latex/*pdf
        - _build/dirhtml
    script:
    - make dirhtml latexpdf

This will generate PDFs and HTML docs. The HTML docs could even be used for
GitLab Pages.
